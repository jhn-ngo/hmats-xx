# HMATS stands for Heritage Metadata Automatic Translation System


## Machine Translation (MT)

Translation is provided by Pangeanic via PangeaMT API and now supports following source languages (target is English):

Language code list could be found [here](https://www.loc.gov/standards/iso639-2/php/code_list.php) or [here](https://lingohub.com/academy/best-practices/iso-639-1-list)

Europeana XX translation languages:

- 'it' (Italian)
- 'de' (German)
- 'cs' (Czech)
- 'el' (Greek)
- 'fr' (French)
- 'sv' (Swedish)
- 'ca' (Catalan)
- 'nl' (Dutch)
- 'pl' (Polish)
- 'es' (Spanish)



---

### How to use MT API?

1. You need to authorize at `https://data.jhn.ngo/translate/auth` in order to use service:

```
curl --location --request POST 'https://data.jhn.ngo/translate/auth' \
--header 'Content-Type: application/json' \
--data-raw '{"user": USERNAME, "password": PASSWORD}'
```

2. The you could actually send request to the service:

```
curl --location --request POST 'https://data.jhn.ngo/translate' \
--header 'Authorization: Bearer YOUR_TOKEN_HERE' \
--header 'Content-Type: application/json' \
--data-raw '{"text":["Soy tu amigo", "Quien eres"], "src":"es", "tgt": "en"}'
```

You should receive response like this:

```
{
    "translation": [
        "I am your friend.",
        "Who are you?"
    ],
    "src": "es",
    "tgt": "en",
    "confidence": 91.44
}
```

### How to deploy application?

1. Install and activate venv from the root directory:

```
cd opt/hmats/
python3 -m venv venv
source venv/bin/activate
```

2. Install requirements:

```
pip install -r requirements.txt
```

3. To launch from supervisor check [supervisor config ](deploy/supervisor.conf). If you want to serve it manually run it with gunicorn:

```
gunicorn api:app --bind 0.0.0.0:4040 --worker-class sanic.worker.GunicornWorker --workers NUMBER_OF_WORKERS
```
