import logging
import sys, os
import tempfile
from sickle import Sickle
from moai.tools import update_moai
from translate.oai.records import EdmRecord
from sickle.iterator import OAIItemIterator
from sickle.models import ResumptionToken
from translate.oai.db import feed_table
import configparser

class ResumableOAIItemIterator(OAIItemIterator):
    def __init__(self, sickle, params, ignore_deleted=False):
        self.initial_resumption_token = params.get('resumptionToken')
        super(ResumableOAIItemIterator, self).__init__(sickle, params, ignore_deleted)

    def _next_response(self):
        # Enforce initial resumption token on the first call to  _next_response()
        if self.initial_resumption_token:
            self.resumption_token = ResumptionToken(token=self.initial_resumption_token)
            self.initial_resumption_token = None
        super(ResumableOAIItemIterator, self)._next_response()


class ResumableOaiTranslator:
    MOAI_SETTINGS_FILE = "translate/oai/settings.ini"

    def __init__(self, database, feed_id, **kwargs):
        self.database = database
        self.feed_id = feed_id
        self.set = kwargs['set']
        self.tmp_dir = tempfile.mkdtemp(prefix=feed_id)
        self.settings_ini = self._create_settings()

        oai = OaiTranslate(**kwargs)
        oai.translate(self.on_translate_batch)

    def on_translate_batch(self, resumption_token, records_batch):
        self._write_batch(records_batch)
        self._update_database(resumption_token)

    def _create_settings(self):
        config = configparser.ConfigParser()
        config.read(self.MOAI_SETTINGS_FILE)
        # Update provider section
        config['app:translate']['provider'] = f"edm://{self.tmp_dir}/{self.feed_id}*.xml"
        config['app:translate']['url'] = f"{os.getenv('OAI_TRANSLATE_URL')}/{self.feed_id}"
        settings_file = os.path.join(
            self.tmp_dir, "settings.ini"
        )
        with open(settings_file, "w") as cf:
            config.write(cf)
        return settings_file

    def _write_batch(self, records):
        if not records: return
        edm_file = os.path.join(self.tmp_dir, f"{self.feed_id}.xml")
        with open(edm_file, "w") as f:
            f.write("<OAI-PMH><ListRecords>")
            for record in records:
                f.write(record.raw)
            f.write("</ListRecords></OAI-PMH>")
        return

    def _update_database(self, resumption_token) :
        sys_argv = sys.argv
        sys.argv = f"update_moai -v -d --set {self.set} --config " \
                   f"{self.settings_ini} translate".split()
        logging.info("Update command: {}".format(" ".join(sys.argv)))
        update_moai()
        # Restore command line
        sys.argv = sys_argv
        # Update resumption tokeb
        self.database.connect()
        query = feed_table.update().where(feed_table.c.id==self.feed_id)
        values = {"resumption_token": resumption_token}
        self.database.execute(query=query, values=values)
        self.database.disconnect()



class OaiTranslate:
    METADATA_PREFIX_MAP = {
        'edm': EdmRecord
    }
    BATCH = 50

    def __init__(self, src, tgt, translator, url, metadataPrefix=None, set=None, resumptionToken=None, pages=1):
        self.translator = translator
        self.url = url
        self.set = set
        self.src = src
        self.tgt = tgt
        # Initialize metadata-specific translator
        if metadataPrefix not in self.METADATA_PREFIX_MAP:
            raise Exception(f"Can't translate OAI feed with metadataPrefix: {metadataPrefix}, available prefixes: {self.METADATA_PREFIX_MAP.keys()}")
        # Initialize OAI-PMH reader
        self.params = dict()
        if metadataPrefix:  self.params['metadataPrefix'] = metadataPrefix
        if set: self.params['set'] = set
        if resumptionToken: self.params['resumptionToken'] = resumptionToken
        self.sickle = Sickle(url, iterator=ResumableOAIItemIterator)
        self.sickle.class_mapping['ListRecords'] = self.METADATA_PREFIX_MAP[metadataPrefix]

        # Find batch size of OAI repo and put correspondent limit based on number of pages
        self.limit = pages * self._calc_oai_batch_size()

    def _calc_oai_batch_size(self):
        records_iter = self.sickle.ListRecords(**self.params)
        init_resumption_token = records_iter.resumption_token
        size = 0
        for r in records_iter:
            if init_resumption_token and init_resumption_token != records_iter.resumption_token:
                break
            size += 1
        return size

    def translate(self, on_translate_batch=lambda x,y: x):
        count = 0
        records = self.sickle.ListRecords(**self.params)
        records_batch = []
        for record in records:
            records_batch.append(record)
            count += 1
            if len(records_batch) >= self.BATCH:
                logging.info(f"Translating {count} records")
                self._translate_batch(self.src, self.tgt, records_batch)
                on_translate_batch(records.resumption_token, records_batch)
                records_batch.clear()
                if self.limit and count >= self.limit:
                    break
        # Translate remainder
        self._translate_batch(self.src, self.tgt, records_batch)
        on_translate_batch(records.resumption_token, records_batch)

    def _translate_batch(self, src, tgt, records):
        # Create lis of translatable texts
        translatable_texts = []
        for record in records:
            translatable_texts += record.get_translatable_texts(src)
        # Prepare actual text strings to translate and translate them
        src_texts = [t.text for t in translatable_texts ]
        tgt_texts = self.translator.translate(src_texts, src, tgt)
        # Put translations back to records
        translated_records = set()
        for tt, tgt_text in zip(translatable_texts, tgt_texts):
            tt.record.set_translation(tt.path, tgt, tgt_text)
            translated_records.add(tt.record)
        # Update raw/xml representation from metadata
        for record in translated_records:
            record.update_xml()

        return records



if __name__ == "__main__":
    import os
    logging.basicConfig(format='%(asctime)s:%(levelname)s:%(message)s', level=logging.DEBUG)
    from translate.translator import Translator
    from dotenv import load_dotenv
    load_dotenv()
    config = {
                "url": os.getenv("ATS_URL"),
                "params": {
                    "apikey": os.getenv("ATS_KEY"),
                    "userapikey": os.getenv("ATS_USER")
                }
             }
    translator = Translator(config)
    oai = OaiTranslate(translator=translator, url='https://data.jhn.ngo/oai', metadataPrefix='edm', set='jhm-museum', limit=100)
    oai.translate('nl', 'en')
