from sickle.models import Record
import xmltodict
import langdetect
from collections import namedtuple, OrderedDict
from urllib.parse import urlparse, parse_qs
from lxml import etree
TranslatableText = namedtuple('TranslatableText', 'record path text')

class EdmRecord(Record):
    TRANSLATE_FIELDS = ['dc:title', 'dc:description', 'dc:subject']
    TEXT_DELIMITER = " ||| "

    def __init__(self, record_element, strip_ns=False):
        super().__init__(record_element, strip_ns)

    def get_metadata(self):
        return xmltodict.parse(self.raw)['record']['metadata']

    # TODO: accept list of translatable fields as a parameter
    def get_translatable_texts(self, lang=None):
        texts = []
        for field in self.TRANSLATE_FIELDS:
            path = ('rdf:RDF', 'edm:ProvidedCHO', field)
            # TODO: use path here too
            value = self.metadata['rdf:RDF']['edm:ProvidedCHO'].get(field)
            tvalue = self._value_to_tt(value)
            # Filter empty texts and texts from other languages
            if not tvalue or lang and langdetect.detect(tvalue) != lang: continue
            texts.append(TranslatableText(record=self, path=path, text=tvalue))
        return texts

    def set_translation(self, path, lang, translation):
        # Create translation entry
        tr_entry = OrderedDict({'@xml:lang': lang,
                                '#text': translation,
                                '@xml:source': 'mt'}) # TODO: make source attribute configurable
        # Find last dictionary in the path
        path_dict = self.metadata
        for key in path[:-1]:
            path_dict = path_dict[key]
        leaf = path_dict[path[-1]]
        # If leaf is already list, append new entry
        if isinstance(leaf, list):
            leaf.append(tr_entry)
        else:
            # Else create new list and concatenate previous content with the new translation
            path_dict[path[-1]] = [leaf, tr_entry]

    def update_xml(self):
        record_json = xmltodict.parse(self.raw)
        record_json['record']['metadata'] = self.metadata
        self.xml = etree.fromstring(xmltodict.unparse(record_json).encode('utf-8'))


    def _value_to_tt(self, value):
        if value is None:
            return
        # Simple string attribute
        elif isinstance(value, str):
            return value
        # Attribute with xml:lang
        elif isinstance(value, dict) and "#text" in value:
            # TODO: validate source language
            return value["#text"]
        # List attribute
        elif isinstance(value, (list, tuple)):
            texts = [self._value_to_tt(v) for v in value]
            texts = [t for t in texts if t] # filter none values
            if not texts: return
            return self.TEXT_DELIMITER.join(texts)

