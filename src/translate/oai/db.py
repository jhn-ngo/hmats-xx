import sqlalchemy


metadata = sqlalchemy.MetaData()

feed_table = sqlalchemy.Table(
    "feed",
    metadata,
    sqlalchemy.Column("id", sqlalchemy.String(length=255), primary_key=True),
    sqlalchemy.Column("url", sqlalchemy.String(length=255)),
    sqlalchemy.Column("metadata_prefix", sqlalchemy.String(length=255)),
    sqlalchemy.Column("set", sqlalchemy.String(length=255)),
    sqlalchemy.Column("resumption_token", sqlalchemy.String(length=255)),
    sqlalchemy.Column("src", sqlalchemy.String(length=3)), # source language
    sqlalchemy.Column("tgt", sqlalchemy.String(length=3)), # target language
)