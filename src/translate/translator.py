import re

import requests
import logging
import time

from sentence_splitter import SentenceSplitter, SentenceSplitterException

# Human Score evaluation
# ----	EN - PangeaMT - Google Translate - Diff
# BG	92.52 88.46 4.1
# CS	91.83 85.86 6.0
# DA	90.22 87.24 3.0
# DE	93.00 89.84 3.2
# EL	90.12 84.07 6.1
# EN	----
# ES	91.44 88.71 2.7
# ET	87.44 83.13 4.3
# FI	91.90 82.17 9.7
# FR	88.84 87.31 1.5
# GA	39.82 35.52 4.3
# HR	74.46 71.63 2.8
# HU	89.65 82.94 6.7
# IT	93.57 90.03 3.5
# LT	87.10 84.61 2.5
# LV	88.72 85.76 3.0
# MT	90.46 88.08 2.4
# NL	82.80 63.48 19.3
# PL	91.46 85.90 5.6
# PT	88.22 70.75 17.5
# RO	88.08 70.89 17.2
# SK	92.84 86.70 6.1
# Sk	----
# SL	90.84 87.37 3.5
# SV	86.39 73.47 12.9
ENGINE_CONFIDENCE = {
    # Major Europeana XX translation pairs
    ('it', 'en'): 93.57,
    ('de', 'en'): 93.00,
    ('cs', 'en'): 91.83,
    ('el', 'en'): 90.12,
    ('fr', 'en'): 88.84,
    ('sv', 'en'): 86.39,
    #('ca', 'en'): TODO
    ('nl', 'en'): 82.80,
    ('pl', 'en'): 88.22,
    ('es', 'en'): 91.44,
    # Other pairs:
    ('bg', 'en'): 92.52,
    ('da', 'en'): 90.22,
    ('et', 'en'): 87.44,
    ('fi', 'en'): 91.90,
    ('ga', 'en'): 39.82,
    ('hr', 'en'): 74.46,
    ('hu', 'en'): 89.65,
    ('lt', 'en'): 87.10,
    ('lv', 'en'): 88.72,
    ('mt', 'en'): 90.46,
    ('pt', 'en'): 88.22,
    ('ro', 'en'): 88.08,
    ('sk', 'en'): 92.84,
    ('sl', 'en'): 90.84
}
DEFAULT_CONFIDENCE = 0.0
MAX_SEGMENT_WORDS = 30
MAX_BATCH_SEGMENTS = 30
TIMEOUT_SECS = 60
SPLIT_PLACEHOLDER = "###"

class Translator:
    def __init__(self, config):
        self.config = config

    def engines(self):
        headers = {"content-type": "application/json"}
        data = self.config.get("params", {})

        try:
            res = requests.post(f'{self.config["url"]}/corp/engines', json=data, headers=headers)
            if res.ok:
                return res.json()["engines"]

        except:
            pass
        return []

    def translate(self, text, src, tgt, engine=None):
        single = False
        if not isinstance(text, (list,tuple)):
            single = True
            text = [text]
        # Split long segments into sentences
        segments,segment_indexes = self._segment_text(text, src)
        # Send segments by batches
        translations = []
        batch_index = 0
        while batch_index < len(segments):
            translations += self._translate_text(segments[batch_index:batch_index+MAX_BATCH_SEGMENTS], src, tgt, engine)
            batch_index += MAX_BATCH_SEGMENTS
        # Join split (translated) segments back into original once
        translations = self._join_segments(translations, segment_indexes)
        assert len(translations) == len(text)

        translations = translations[0] if single else translations
        return {"translation": translations,
                "src": src,
                "tgt": tgt,
                "confidence": ENGINE_CONFIDENCE.get((src,tgt), DEFAULT_CONFIDENCE) }

    def _segment_text(self, text_list, lang):
        segment_list = []
        segment2orig_indexes = []
        try:
            sent_splitter = SentenceSplitter(language=lang)
        except SentenceSplitterException as e:
            sent_splitter = SentenceSplitter(language="en") # Default splitter for English if no splitter for input language exist
        # Split long texts into sentences
        for text in text_list:
            text = re.sub('\n', SPLIT_PLACEHOLDER, text)
            start_index = len(segment_list)
            if len(text.split()) > MAX_SEGMENT_WORDS:
                split_sentences = self._reduce_split(sent_splitter.split(text))
                # TODO: handle corner case if the sentence is still too long
                segment_list += split_sentences
            else:
                segment_list.append(text)
            segment2orig_indexes.append((start_index, len(segment_list)))
        # Replace back split placeholder
        segment_list = [re.sub(SPLIT_PLACEHOLDER, '\n', s) for s in segment_list]
        return segment_list,segment2orig_indexes

    # Joining short sentences together
    def _reduce_split(self, sentences):
        reduced_segments = []
        joined_sentences = ""
        for sent in sentences:
            len_words = len(sent.split())
            if not joined_sentences or len(joined_sentences.split()) + len_words <= MAX_SEGMENT_WORDS:
                joined_sentences += " " + sent if not joined_sentences else sent
            else:
                reduced_segments.append(joined_sentences)
                joined_sentences = ""
        # Last one (if left)
        if joined_sentences:
            reduced_segments.append(joined_sentences)
        return reduced_segments

    def _join_segments(self, segments, segment2orig_indexes):
        out_segments = []
        for si,ei in segment2orig_indexes:
            out_segments.append(" ".join(segments[si:ei]))
        return out_segments

    def _translate_text(self, text, src, tgt, engine):
        logging.info(f"Translating {src} to {tgt}, number of segments: {len(text)} ")
        # logging.info(f"Translating {text} ")

        data = {"src":src,
                "tgt" : tgt,
                "mode": 2,
                "text": text
                }
        if engine:
            data["engine"] = engine
        headers = {"content-type": "application/json"}
        data.update(self.config.get("params", {}))

        try:
            start_time = time.time()
            res = requests.post(f'{self.config["url"]}/translate', json=data, headers=headers, timeout=(5, TIMEOUT_SECS))
            diff_time = time.time() - start_time
            logging.info(f"Translated {src} to {tgt}, number of segments: {len(text)}, time: {round(diff_time,2)}s, rate: {round(len(text)/diff_time,2)} segments/sec")
            # timeouts docs: https://docs.python-requests.org/en/master/user/advanced/#timeouts
            if res.ok:
                res = res.json()
            else:
                raise Exception(f"Failed to translate: {text}")
        except Exception as e:
            raise e

        translations = [r[0]["tgt"] for r in res]
        # logging.info(f"Translation {translations} ")
        return translations

if __name__ == "__main__":
    import sys, os
    from dotenv import load_dotenv
    load_dotenv()
    config = {
                "url": os.getenv("ATS_URL"),
                "params": {
                    "apikey": os.getenv("ATS_KEY"),
                    "userapikey": os.getenv("ATS_USER")
                }
             }
    translator = Translator(config)
    if len(sys.argv) < 2:
       print("Usage: translate.py <src>-<tgt> [text]")
       exit
    if len(sys.argv) > 2:
       text = sys.argv[2]
    else:
       text = "Try not to become a man of success but rather to become a man of value. Albert Einstein"
    src,tgt = sys.argv[1].split('-')
    print(translator.translate(text, src, tgt))


