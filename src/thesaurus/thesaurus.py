import sys, re
import logging
import skosify  # contains skosify, config, and infer
#import spacy

query = """
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>

SELECT DISTINCT ?concept ?prefLabel
WHERE { 
      ?concept skos:prefLabel ?prefLabel .
}

"""
#      ?concept skos:broader+ <http://culturaitalia.it/pico/thesaurus/4.1#beni_culturali> .

#      FILTER NOT EXISTS { ?concept skos:narrower ?child }

# @[property-heritageObject-type]
#     ~[book]
#     ~[painting]
#     ~[photograph]
#     ~[document]
#     ~[artwork]
#     ~[postcard]
#     ~[paper]
#     ~[letter]
#     ~[webpage]
#
# ~[book]
#     book
#     books
#     writer
#     writers
#     poet
#     poets

class Thesaurus:
    def __init__(self, skos_file):
        self.voc = skosify.skosify(skos_file, label='Ontology')

    def to_glossary(self, lang1, lang2):
        res = self.voc.query(query)
        keywords = dict()
        for r in res:
            key = r[0].toPython()
            language = r[1].language
            if not key in keywords:
                keywords[key] = [None, None]
            index = 0 if language == lang1 else 1 if language == lang2 else None
            if index is None: continue
            keywords[key][index] = r[1].toPython()
        return keywords


def skos2dict(skos_file, in_language, out_language):
    the = Thesaurus(skos_file)
    res = the.voc.query(query)
    keywords = dict()
    for r in res:
        print("R: {}".format(r))
        language = r[1].language
        if language == in_language:
            concept = r[1].toPython().lower()
            #print("CONCEPT: {}".format(r[0]))
            concept_key = re.sub("[^A-z0-9]+", '-', concept)
            keywords[r[0].toPython()] = concept_key
    return keywords

if __name__ == "__main__":
    #skos2chatito(sys.argv[1], sys.argv[2])
    print(skos2dict(sys.argv[1], sys.argv[2], sys.argv[3]))
