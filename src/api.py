import os
import tempfile
import logging
import simplejson
import random
import string
import langdetect

from dotenv import load_dotenv

from sanic import Sanic, Blueprint
from sanic.response import text, json, empty
from sanic.views import HTTPMethodView
from sanic.exceptions import abort


from sanic_openapi import doc, swagger_blueprint, api
from sanic_jwt import Initialize, protected, exceptions
from sanic_jwt.endpoints  import AuthenticateEndpoint

from langdetect import detect_langs
from langdetect.lang_detect_exception import LangDetectException

from thesaurus.thesaurus import Thesaurus
from translate.translator import Translator
from translate.oai.oai_translate import ResumableOaiTranslator
from moai.wsgi import app_factory
from webob.request import environ_from_url
from configparser import ConfigParser
from translate.oai.db import feed_table, metadata
from databases import Database
from sqlalchemy import create_engine

import threading
from urllib.parse import urlencode, urlparse, urlunparse, parse_qsl

app = Sanic()


def create_blueprint(name):
    prefix = "/"
    return Blueprint(f"{name}", url_prefix=f"{prefix}{name}")

#bp_auth = create_blueprint("auth")
bp_glossaries = create_blueprint("thesauruses")
bp_translate = create_blueprint("translate")

class User:
    user = str
    password = str

class Token:
    token = doc.String(description='JWT token received from /auth endpoint')

class TranslationBatch:
    engine = doc.String(description="Engine ID", required=False)
    src = doc.String(description='Source language (2-letters ISO)')
    tgt = doc.String(description='Target language (2-letters ISO)')
    text = doc.List(str, description='List of text segments to translate')

class LangdetectBatch:
    text = doc.List(str, description='List of text segments to detect language')


TOKEN="authorization"


# Load and init translation engine
load_dotenv()
config = {
    "url": os.getenv("ATS_URL"),
    "params": {
        "apikey": os.getenv("ATS_KEY"),
        "userapikey": os.getenv("ATS_USER")
    }
}
translator = Translator(config)

# Initialize database
db_url = 'sqlite:///edm.db'
metadata.create_all(create_engine(db_url, echo = True))
database = Database(db_url)

# Authentication method (TODO: implement user storage
creds = {cred for cred in os.getenv("CREDENTIALS").split()}
async def authenticate(request):
    if not creds:
        raise exceptions.AuthenticationFailed("No credentials defined")
    if not request.json:
        raise exceptions.AuthenticationFailed("User and password are required")

    username = request.json.get("user", None)
    password = request.json.get("password", None)
    if f'{username}:{password}' not in creds:
        raise exceptions.AuthenticationFailed()
    return dict(user_id=username)

# add swagger doc to sanic JWT method
AuthenticateEndpoint.decorators.extend([
    doc.summary("Authenticate user and get token"),
    doc.consumes(User, location='body'),
    doc.produces(Token),
])

jwt = Initialize(app, authenticate=authenticate,
                 expiration_delta=60*60*24) # validity of the token 24 hours

app.blueprint(swagger_blueprint)

# class AuthView(HTTPMethodView):
#     @doc.consumes(User, location="body")
#     @doc.produces(Token)
#     @doc.summary("Authorize API with username and password. Returns token")
#     def post(self, request):
#         return json({TOKEN: "dfgfhggdfg"})


class ThesaurusView(HTTPMethodView):
    decorators = [protected()]

    @bp_glossaries.get("/<thesaurus_id:str>")
    @doc.summary("Get thesaurus by id")
    @doc.consumes(doc.String(name=TOKEN), location="header", required=True)
    @protected()
    def get_one(self, request, thesaurus_id):
        return text("I am get method")

    @doc.summary("Lists all thesauruses")
    @doc.consumes(doc.String(name=TOKEN), location="header", required=True)
    @doc.consumes(doc.Integer(name="offset"))
    @doc.consumes(doc.Integer(name="limit"))
    def get(self, request):
        return text("I am get method")

    @doc.summary("Upload a thesaurus")
    @doc.consumes(doc.File(name="file"), location="formData", content_type="multipart/form-data")
    @doc.consumes(doc.JsonBody({
        "content" : doc.String("Content of thesaurus (SKOS)")}), location="body")
    @doc.consumes(doc.String(name=TOKEN), location="header", required=True)
    def post(self, request):
        file = request.files.get("file")
        fname = tempfile.mktemp(prefix=file.name)
        with open(fname, 'wb') as f:
            f.write(file.body)
        the = Thesaurus(fname)
        glossary = the.to_glossary(lang1='it', lang2='en')
        return json(glossary)

    @bp_glossaries.delete("/<thesaurus_id:str>")
    @doc.summary("Delete a thesaurus")
    @doc.consumes(doc.String(name=TOKEN), location="header", required=True)
    @protected()
    def delete_one(self, request, thesaurus_id):
        return text("I am delete method")

class TranslateOaiFeed:
    url = doc.String(description="OAI endpoint or full OAI url, including metadataPrefix and set")
    metadataPrefix = doc.String(description="Metadata prefix", required=False)
    set = doc.String(description="OAI dataset", required=False)
    src = doc.String(description='Source language (2-letters ISO)')
    tgt = doc.String(description='Target language (2-letters ISO)')


class TranslateView(HTTPMethodView):
    decorators = [protected()]
    # TODO: configure per user
    # Italian, German, Czech, Greek, French, Swedish, Catalan, Dutch, Polish, and Spanish -> to English
    language_pairs = [('it', 'en'),
                      ('de', 'en'),
                      ('cs', 'en'),
                      ('el', 'en'),
                      ('fr', 'en'),
                      ('sv', 'en'),
                      ('ca', 'en'),
                      ('nl', 'en'),
                      ('pl', 'en'),
                      ('es', 'en'),
                      ]
    threads_map = dict()

    @doc.summary("Translate list of text segments")
    @doc.consumes(doc.String(name=TOKEN), location="header", required=True)
    @doc.consumes(TranslationBatch, location="body", required=True)
    def post(self, request):
        batch = simplejson.loads(request.body)
        if not batch['text'] or not batch['tgt']:
            abort(403, "Fields 'text' and 'tgt' are mandatory in the body")
        # Detect language if it was not provided
        src_lang_confidence = None
        if not self._validate_src_lang(batch.get('src'), batch['tgt']):
            batch['src'], src_lang_confidence = TranslateView._detect_language(batch['text'])
            if not batch['src']:
                return abort(403, f"Cannot detect language for provided text")

        batch['src'] = batch['src'].lower()
        batch['tgt'] = batch['tgt'].lower()

        if  batch['src'] == batch['tgt']:
            translation_result = {"translation": batch['text'],
                    "src": batch['src'],
                    "tgt": batch['tgt'],
                    "confidence": src_lang_confidence if src_lang_confidence else 1.0
            }
            if src_lang_confidence:
                translation_result["src_lang_confidence"] = src_lang_confidence
            return json(translation_result)
        try:
            translation_result = translator.translate(batch['text'], batch['src'], batch['tgt'], batch.get('engine'))
        except Exception as e:
            logging.error(f"Failed to translate batch: {batch}, error: {e}")
            return empty(status=413)

        if src_lang_confidence:
            translation_result["src_lang_confidence"] = src_lang_confidence
        return json(translation_result)

    def _validate_src_lang(self, src, tgt):
        if not src or not (src.lower(),tgt.lower()) in self.language_pairs:
            return False
        return True

    @staticmethod
    @bp_translate.post("/oai")
    @doc.summary("Generate translated OAI feed")
    @doc.consumes(doc.String(name=TOKEN), location="header", required=True)
    @doc.consumes(TranslateOaiFeed, location="body", required=True)
    @protected()
    async def post_oai(request):
        oai_feed = simplejson.loads(request.body)
        # Generate new id for given feed
        id = ''.join(random.choice(string.ascii_lowercase) for i in range(10))

        # Parse given OAI feed URL and override any parameter given explicitly
        url_parsed = urlparse(oai_feed['url'])
        feed_dict = dict(parse_qsl(url_parsed.query))
        feed_dict.update(oai_feed)
        feed_dict['url'] =  urlunparse(url_parsed._replace(query=''))
        # Insert new entry to the feed table
        # Establish the connection pool
        await database.connect()
        query = feed_table.insert()
        values = {"id": id,
                  "url": feed_dict['url'],
                  "metadata_prefix": feed_dict['metadataPrefix'],
                  "set": feed_dict['set'],
                  "src": feed_dict['src'],
                  "tgt": feed_dict['tgt']}
        await database.execute(query=query, values=values)
        await database.disconnect()

        # Start translation in a separate thread
        thread_args = {'database': database,
                       'feed_id': id,
                       'translator': translator,
                       'url': feed_dict['url'],
                       'metadataPrefix': feed_dict['metadataPrefix'],
                       'set': feed_dict['set'],
                       'pages': 2, # cache size
                       'src': feed_dict['src'],
                       'tgt': feed_dict['tgt']}
        t = threading.Thread(target=ResumableOaiTranslator, kwargs=thread_args)
        t.start()
        TranslateView.threads_map[id] = t

        feed_params = {'verb': 'ListRecords',
                  'metadataPrefix': feed_dict['metadataPrefix'],
                  'set': feed_dict['set']}
        translate_url = f"{request.scheme}://{request.host}{request.path}/{id}?{urlencode(feed_params)}"
        return json({'translate_url': translate_url}, escape_forward_slashes=False)

    @staticmethod
    @bp_translate.get("/oai/<oai_feed_id>")
    @doc.summary("Generate translated OAI feed")
    @doc.consumes(doc.String(name=TOKEN), location="header", required=True)
    async def get_oai(request, oai_feed_id):
        # Validate feed id
        await database.connect()
        query = feed_table.select().where(feed_table.c.id==oai_feed_id)
        feed_row = await database.fetch_one(query=query)
        await database.disconnect()
        if not feed_row:
            abort(403, "Translated feed doesn't exist. Please get it by posting to /translate/oai")

        # If previously run thread is still running wait for it
        if oai_feed_id in TranslateView.threads_map:
            TranslateView.threads_map[oai_feed_id].join()

        # Trigger harvesting thread in the background to cache additional page
        thread_args = {'database': database,
                       'feed_id': oai_feed_id,
                       'translator': translator,
                       'url': feed_row['url'],
                       'set': feed_row['set'],
                       'metadataPrefix': feed_row['metadata_prefix'],
                       'src': feed_row['src'],
                       'tgt': feed_row['tgt'],
                       'resumptionToken': feed_row['resumption_token']}
        t = threading.Thread(target=ResumableOaiTranslator, kwargs=thread_args)
        t.start()
        TranslateView.threads_map[oai_feed_id] = t

        # Read config and initialize MOAI WSGI app accordingly
        config_file = "translate/oai/settings.ini"
        config = ConfigParser()
        config.read(config_file)
        config['app:translate']['url'] = f'{os.getenv("OAI_TRANSLATE_URL")}/{oai_feed_id}'
        conf = dict(config['app:translate'].items())
        oai_app = app_factory(None, **conf)
        # Workaround: setting batch size. TODO: modify app_factory to pass batch_size further to the OAI server
        oai_app.server._config.batch_size = 25
        # Placeholder callback, needed for WSGI
        def start_response(status, response_headers, exc_info=None):pass
        # FIXME: dirty hardcoded scheme to avoid unexplainable missing forwaded headers"
        request.forwarded['proto'] = urlparse(os.getenv("OAI_TRANSLATE_URL")).scheme 
        logging.info(f"Forwarded: {request.forwarded}")
        # Delegate OAI hanlding to MOAI
        response = oai_app(dict(environ_from_url(request.url)), start_response)
        logging.info(f"Feed {request.url} {oai_feed_id} -> {response}")
        return text(response[0], content_type="text/xml")


    @bp_translate.get("/engines")
    @doc.summary("List all available translation engines")
    @doc.consumes(doc.String(name=TOKEN), location="header", required=True)
    @protected()
    def list_engines(self):
        filtered_engines = []
        for engine in translator.engines():
            if engine['flavor']: continue
            lang_pair = (engine['src'], engine['tgt'])
            if lang_pair in TranslateView.language_pairs:
                f_engine = engine # TODO: filter fields: {k: v for k,v in engine.items() if k in ["id", "src", "tgt"]}
                filtered_engines.append(f_engine)
        return json({'engines': filtered_engines})

    @staticmethod
    @bp_translate.post("/langdetect")
    @doc.summary("Detect language for give text segment(s)")
    @doc.consumes(doc.String(name=TOKEN), location="header", required=True)
    @doc.consumes(LangdetectBatch, location="body", required=True)
    def langdetect(request):
        req = simplejson.loads(request.body)
        text = req.get('text')
        if not text:
            abort(403, "Field 'text' and 'tgt' is mandatory in the body")
        (lang, confidence) = TranslateView._detect_language(text)
        return json({'language': lang, 'confidence': confidence})

    @staticmethod
    def _detect_language(text_list):
        text = " ".join(text_list) if isinstance(text_list, (list,tuple)) else text_list
        try:
            detected_langs = detect_langs(text)
        except LangDetectException:
            # if language cannot be detected set engl
            return (None, None)
        source_lang, score = (detected_langs[0].lang, round(detected_langs[0].prob*100, 2))
        return (source_lang, score)


def add_blueprint(bp, view):
    bp.add_route(view.as_view(), "/")
    app.blueprint(bp)

#add_blueprint(bp_auth, AuthView)
add_blueprint(bp_glossaries, ThesaurusView)
add_blueprint(bp_translate, TranslateView)

if __name__ == "__main__":
    from multiprocessing import cpu_count
    logging.basicConfig(format='%(asctime)s:%(levelname)s:%(message)s', level=logging.DEBUG)
    app.run(host="0.0.0.0", port=4040, workers=cpu_count()-1)
