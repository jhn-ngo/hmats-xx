import React from "react";
import ReactDOM from "react-dom";
import { Formik, Field, Form } from "formik";
import "./styles.css";

function App() {
  return (
    <div className="App">
      <h1>Contact Us</h1>
      <Formik
        initialValues={{ oai_url: "", src: "" }}
        onSubmit={async values => {
          await new Promise(resolve => setTimeout(resolve, 500));
          alert(JSON.stringify(values, null, 2));
        }}
      >
        <Form>
          <label htmlFor="oai_url">OAI-PMH EDM feed</label>
          <Field name="oai_url" type="url" />
          <label htmlFor="src">Translate from</label>
                <Field component="select" id="src" name="src" className={"form-control"} >
                   <option value="it">Italian</option>
                   <option value="ca">Catalan</option>
                   <option value="de">German</option>
                   <option value="es">Spanish</option>
                   <option value="gr">Greek</option>
                   <option value="fr">French</option>
                   <option value="cz">Czech</option>
                   <option value="sv">Swedish</option>
                   <option value="nl">Dutch</option>
                   <option value="pl">Polish</option>
                </Field>
          <button type="submit">Submit</button>
        </Form>
      </Formik>
    </div>
  );
}

ReactDOM.render(<App />, document.getElementById("root"));
